--USERS
-- 1
INSERT INTO users (email,password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");

INSERT INTO users (email,password,datetime_created) VALUES ("juandelecruz@gmail.com", "passwordB", "2022-11-09 02:00:00");

INSERT INTO users (email,password,datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2022-11-09 03:00:00");

INSERT INTO users (email,password,datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2022-11-09 04:00:00");

INSERT INTO users (email,password,datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2022-11-09 05:00:00");

DELETE FROM users;
ALTER TABLE posts AUTO_INCREMENT = 0;

DELETE FROM posts;
ALTER TABLE posts AUTO_INCREMENT = 0;

-- 2
-- posts
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, 'First Code' , 'Hello World', "2021-01-02 01:00:00");

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, 'Second Code' , 'Hello Earth', "2021-01-02 02:00:00");

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (2, 'Third Code' , 'Welcome ti Mars!', "2021-01-02 03:00:00");

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (4, 'Fourth Code' , 'Bye bye solar system!', "2021-01-02 04:00:00");

-- 3
SELECT * FROM posts WHERE author_id = 1;
-- 4
SELECT email, datetime_created FROM users;
-- 5
UPDATE posts SET content = "Hello to the people of the Earth" WHERE id =2;
-- 6
DELETE FROM users WHERE email = 'johndoe@gmail.com';

